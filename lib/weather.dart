import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(weather());
}

var divider = Divider(
  color: Colors.grey,
);

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        ));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        ));
  }
}

// var appbar = AppBar(
//     leading: Icon(
//       Icons.arrow_back,
//       // color: Colors.black,
//     ),
//     actions: <Widget>[
//       IconButton(
//           icon: Icon(Icons.star_border),
//           // color: Colors.black,
//           onPressed: () {
//             print("Contact is starred");
//           })
//     ],
//     );

var body = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 50, 5, 5),
                  child: Text(
                    "อ.เมืองชลบุรี",
                    style: TextStyle(fontSize: 30),
                  )),
              Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    "26 ํ",
                    style: TextStyle(fontSize: 60),
                  )),
              Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    "เมฆเป็นส่วนมาก",
                    style: TextStyle(fontSize: 20),
                  )),
              Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    "สูงสุด: 30 ํ ต่ำสุด: 20 ํ",
                    style: TextStyle(fontSize: 20),
                  )),
            ],
          ),
        ),
        divider,
        Container(
          // color: Colors.white.withOpacity(0.3),
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("คาดว่ามีเมฆเป็นบางส่วนประมาณเวลา 20:00",
                    style: TextStyle(fontSize: 15)),
              ],
            ),
            divider,
            todayWeather(),
          ]),
        ),
        divider,
        Container(
          // color: Colors.white.withOpacity(0.3),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.calendar_month, color: Colors.white),
                  Text("พยากรณ์อากาศ 10 วัน", style: TextStyle(fontSize: 15)),
                  divider,
                ],
              ),
              today(),
              divider,
              monday(),
              divider,
              tuesday(),
              divider,
              wednesday(),
              divider,
              thursday(),
              divider,
              friday(),
              divider,
              saturday(),
              divider,
              sunday(),
              divider,
            ],
          ),
        ),
      ],
    )
  ],
);

// var apps = BottomNavigationBar(
//   items: const <BottomNavigationBarItem>[
//     BottomNavigationBarItem(
//       icon: Icon(
//         Icons.map_outlined,
//       ),
//       label: 'แผนที่',
//     ),
//     BottomNavigationBarItem(
//       icon: Icon(Icons.density_medium),
//       label: 'ตัวเลือก',
//     ),
//   ],
// );

class weather extends StatefulWidget {
  @override
  State<weather> createState() => _weather();
}

class _weather extends State<weather> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: currentTheme == APP_THEME.DARK
            ? MyAppTheme.appThemeLight()
            : MyAppTheme.appThemeDark(),
        home: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("./img/background.jpg"), fit: BoxFit.cover),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: body,
            // floatingActionButton: FloatingActionButton(
            //   child: Icon(Icons.threesixty),
            //   onPressed: () {
            //     setState(() {
            //       currentTheme == APP_THEME.DARK
            //           ? currentTheme = APP_THEME.LIGHT
            //           : currentTheme = APP_THEME.DARK;
            //     });
            //   },
            // ),
            // backgroundColor: Colors.blue,
            // bottomNavigationBar: apps,
          ),
        ));
  }
}

Widget timeNow() {
  return Column(
    children: <Widget>[
      Text("ตอนนี้"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("25 ํ"),
    ],
  );
}

Widget time20pm() {
  return Column(
    children: <Widget>[
      Text("20"),
      IconButton(
        icon: Icon(Icons.cloudy_snowing, color: Colors.white),
        onPressed: () {},
      ),
      Text("24 ํ"),
    ],
  );
}

Widget time21pm() {
  return Column(
    children: <Widget>[
      Text("21"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("23 ํ"),
    ],
  );
}

Widget time22pm() {
  return Column(
    children: <Widget>[
      Text("22"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("22 ํ"),
    ],
  );
}

Widget time23pm() {
  return Column(
    children: <Widget>[
      Text("23"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("22 ํ"),
    ],
  );
}

Widget time00am() {
  return Column(
    children: <Widget>[
      Text("00"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("21 ํ"),
    ],
  );
}

Widget time01am() {
  return Column(
    children: <Widget>[
      Text("01"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("20 ํ"),
    ],
  );
}

Widget time02am() {
  return Column(
    children: <Widget>[
      Text("02"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("19 ํ"),
    ],
  );
}

Widget time03am() {
  return Column(
    children: <Widget>[
      Text("03"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("19 ํ"),
    ],
  );
}

Widget time04am() {
  return Column(
    children: <Widget>[
      Text("04"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("18 ํ"),
    ],
  );
}

Widget time05am() {
  return Column(
    children: <Widget>[
      Text("05"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("19 ํ"),
    ],
  );
}

Widget time06am() {
  return Column(
    children: <Widget>[
      Text("06"),
      IconButton(
        icon: Icon(Icons.cloud, color: Colors.white),
        onPressed: () {},
      ),
      Text("19 ํ"),
    ],
  );
}

Widget today() {
  return ListTile(
    leading:Text("วันนี้        ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("23° -> 31°"
    ) ,
    onTap: () {

    },
  );

}

Widget monday() {
  return ListTile(
    leading:Text("จ.           ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );


}

Widget tuesday() {
  return ListTile(
    leading:Text("อ.           ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("23° -> 31°"
    ) ,
    onTap: () {

    },
  );

}

Widget wednesday() {
  return ListTile(
    leading:Text("พ.           ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}

Widget thursday() {
  return ListTile(
    leading:Text("พฤ.          ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}

Widget friday() {
   return ListTile(
    leading:Text("ศ.           ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}

Widget saturday() {
   return ListTile(
    leading:Text("ส.           ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}

Widget sunday() {
   return ListTile(
    leading:Text("อา.           ") ,
    title: Icon(Icons.cloud,color: Colors.white,),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}

Widget todayWeather() {
  return Container(
    child: SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          timeNow(),
          time20pm(),
          time21pm(),
          time22pm(),
          time23pm(),
          time00am(),
          time01am(),
          time02am(),
          time03am(),
          time04am(),
          time05am(),
          time06am(),
        ],
      ),
    ),
  );
}
